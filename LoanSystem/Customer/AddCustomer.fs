namespace LoanSystem.Customer

open LoanSystem.CommonTypes.DomainErrors
open LoanSystem.Customer.Customer

module AddCustomer =
    
    let execute (x:CustomerDto) : Result<CreatedCustomerDto, DomainErrors> =
        Customer.create x
        |> Result.bind(fun createdCustomer ->
            Ok <| Customer.createCreatedCustomerDto createdCustomer)

