namespace LoanSystem.Customer

open LoanSystem.CommonTypes
open LoanSystem.CommonTypes.DomainErrors

module Customer =
    
    type CustomerDto = {
        FirstName: string
        MiddleName: string option
        LastName: string
        Age: int
    }
    
    type CreatedCustomerDto = {
        Id: int
        FirstName: string
        MiddleName: string option
        LastName: string
        Age: int
    }
    
    type Age = private Age of int
    
    type Customer = private {
        FirstName: NonEmptyString.NonEmptyString
        MiddleName: NonEmptyString.NonEmptyString option
        LastName: NonEmptyString.NonEmptyString
        Age: Age
    }
    
    let createAge x =
        match x with
            | a when a < 16 -> Error Under16
            | _ -> Ok <| Age x
            
    let evaluateAge (Age x) =
       x
    
    let create (x:CustomerDto) : Result<Customer, DomainErrors>  =
        NonEmptyString.create x.FirstName
        |> Result.bind(fun firstName ->
                NonEmptyString.create x.LastName
                |> Result.bind(fun lastName ->
                    createAge x.Age
                    |> Result.bind(fun age ->
                        match x.MiddleName with
                                | None ->  Ok {
                                        FirstName = firstName
                                        LastName = lastName
                                        Age = age
                                        MiddleName = None
                                 }
                                | Some y ->
                                    NonEmptyString.create y
                                    |> Result.bind(fun middleName ->
                                            Ok {
                                            FirstName = firstName
                                            LastName = lastName
                                            Age = age
                                            MiddleName = Some middleName
                                            }
                                        )
                        )
                    )
            )
        
    let createCreatedCustomerDto (x: Customer) : CreatedCustomerDto =
        let firstName = NonEmptyString.evaluate x.FirstName
        let lastName = NonEmptyString.evaluate x.LastName
        let middleName = match x.MiddleName with
                | None -> None
                | Some y -> Some <| NonEmptyString.evaluate y
        let age = evaluateAge x.Age
        
        {
            Id = 1
            FirstName = firstName
            LastName = lastName
            MiddleName = middleName
            Age = age
        }

