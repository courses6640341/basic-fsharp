namespace LoanSystem.CommonTypes

module DomainErrors =
    
    type DomainErrors =
        | Under16
        | EmptyString

