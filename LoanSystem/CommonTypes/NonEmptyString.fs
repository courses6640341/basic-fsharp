namespace LoanSystem.CommonTypes

open System
open DomainErrors

module NonEmptyString =

    type NonEmptyString = private NonEmptyString of string
    
    let create x =
        if String.IsNullOrWhiteSpace x then
            Error EmptyString
        else
            Ok <| NonEmptyString x
            
    
    let evaluate (NonEmptyString x) =
        x