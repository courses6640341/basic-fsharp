﻿open LoanSystem.Customer

let invalidCustomerDto : Customer.CustomerDto = {
    FirstName = "Halberto"
    LastName = "Silva"
    MiddleName = None
    Age = 15
}

let validCustomerDto : Customer.CustomerDto= {
    FirstName = "Halberto"
    LastName = "Silva"
    MiddleName = None
    Age = 21
}

let validCustomer = AddCustomer.execute validCustomerDto
match validCustomer with
| Ok _ -> printfn "Ok 01"
| Error error -> printfn $"%A{error}"

let invalidCustomer = AddCustomer.execute  invalidCustomerDto
match invalidCustomer with
| Ok _ -> printfn "Ok 02"
| Error error -> printfn $"%A{error}"